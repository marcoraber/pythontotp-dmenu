# pythonTOTP-dmenu

Python script that copies in the clipboard the selected one-time password code

# Dependencies:
* suckless's dmenu
* [python dmenu](https://pypi.org/project/dmenu/)
* [pythonTOTP](https://pypi.org/project/pyotp/)
* [clipboard](https://pypi.org/project/clipboard/)

# Usage:
Download the script, make it executable (`chmod +x /path/to/pythonTOTP-dmenu)
make a file named `keys` in the same directory (or change the code that references that file)
`keys` has this structure:

`Name of the site`

`TOTP KEY GIVEN BY THE WEBSITE`

`Name of another site`

`TOTP KEY OF THE SECOND WEBSITE`

`...`

The rows with the name is what you'll see in dmenu when you launch this script
